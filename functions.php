

<?php 
// Adicionar suporte a Tema no Site
add_theme_support( 'custom-logo', array(
    'height'      => 200,
    'width'       => 400,
    'flex-height' => true,
    'flex-width'  => true,
    'header-text' => array( 'site-title', 'site-description' ),
));
//Adicionar suporte Beckgroud 
    add_theme_support( 'custom-background' );
     wp_enqueue_style( 'style', get_stylesheet_uri() );
  #Adicionar Imagem Destacada
    add_theme_support("post-thumbnails");

    wp_enqueue_script("jquery");
?>

